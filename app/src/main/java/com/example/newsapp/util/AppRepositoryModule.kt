package com.example.newsapp.util

import com.example.newsapp.service.ApiCaller
import org.koin.dsl.module

val appRepositoryModule = module {
    single { ApiCaller(get()) }
}