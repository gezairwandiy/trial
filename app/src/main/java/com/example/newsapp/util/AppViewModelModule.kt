package com.example.newsapp.util

import com.example.newsapp.viewmodel.SourceViewModel
import org.koin.dsl.module

val appViewModelModule = module {
    single { SourceViewModel(get())}
}