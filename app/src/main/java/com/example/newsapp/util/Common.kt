package com.example.newsapp.util

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.example.newsapp.R

object FragmentController{
    fun navigateTo(context: FragmentActivity,
                   fragmentName: String,
                   args: Bundle? = null,
                   addToBackStack: Boolean = false,
                   reorderingAllowed: Boolean = false) {
        updateFragmentTransaction(context, fragmentName, args, addToBackStack, reorderingAllowed)
    }

    private fun updateFragmentTransaction(context: FragmentActivity?, fragmentName: String, args: Bundle?, addToBackStack: Boolean, reorderingAllowed: Boolean) {
        val ft = context?.supportFragmentManager?.beginTransaction()
        val fragment = androidx.fragment.app.Fragment.instantiate(context!!, fragmentName, args)
        ft?.replace(R.id.container, fragment, fragmentName)
        if (addToBackStack) ft?.addToBackStack(fragmentName)
        if (reorderingAllowed) ft?.setReorderingAllowed(true)
        ft?.commitAllowingStateLoss()
    }

}