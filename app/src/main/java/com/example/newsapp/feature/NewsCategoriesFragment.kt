package com.example.newsapp.feature

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newsapp.R
import com.example.newsapp.databinding.FragmentNewsCategoriesBinding
import com.example.newsapp.feature.base.BaseFragment
import com.example.newsapp.feature.base.GenericAppAdapter
import com.example.newsapp.viewmodel.SimpleViewModel
import com.example.newsapp.viewmodel.SourceViewModel
import kotlinx.android.synthetic.main.fragment_news_categories.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class NewsCategoriesFragment : BaseFragment() {

    private val sourceViewModel: SourceViewModel by sharedViewModel()
    private lateinit var binding: FragmentNewsCategoriesBinding
    private lateinit var adapter: GenericAppAdapter<SimpleViewModel>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_categories, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = sourceViewModel
        setupPulltoRefresh()
        pull_to_refresh.setRefreshing(true)
        getData()
    }

    fun setupPulltoRefresh() {
        pull_to_refresh.setOnRefreshListener {
            pull_to_refresh.postDelayed(
                {
                    getData()
                    pull_to_refresh.setRefreshing(true)
                }, 2000
            )
        }
    }

    fun getData() {
        sourceViewModel.getSources( {
            setupList()
            pull_to_refresh.setRefreshing(false)
        }, {
            pull_to_refresh.setRefreshing(false)
        })
    }


    fun setupList() {
        if (sourceViewModel.list.isNotEmpty()) {
            tv_no_data.visibility = View.VISIBLE
            adapter = GenericAppAdapter(sourceViewModel.list)
            val linearLayoutManager = LinearLayoutManager(mActivity)
            rv_news_categories.layoutManager = linearLayoutManager
            rv_news_categories.adapter = adapter
        } else {
            tv_no_data.visibility = View.VISIBLE

        }
    }
}