package com.example.newsapp.feature.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.newsapp.BR
import com.example.newsapp.viewmodel.SimpleViewModel
import java.util.ArrayList

open class GenericViewHolder<out V : ViewDataBinding>(private val viewDataBinding: V) : RecyclerView.ViewHolder(viewDataBinding.root) {

    fun getBinding(): V {
        return viewDataBinding
    }

}

open class GenericAppAdapter<T>(var list: List<T>) : ListAdapter<T, GenericViewHolder<*>>(BaseCallback()) {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return (list[position] as SimpleViewModel).layoutId()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder<*> {
        val bind = DataBindingUtil.bind<ViewDataBinding>(LayoutInflater.from(parent.context).inflate(viewType, parent, false))
            ?: throw IllegalArgumentException()
        return GenericViewHolder(bind)
    }

    override fun onBindViewHolder(holder: GenericViewHolder<*>, position: Int) {
        val model = list[position]
        holder.getBinding().setVariable(BR.rowViewModel, model)
        holder.getBinding().executePendingBindings()
    }

    // This is for swipe to delete button
    private fun removeAt(position: Int) {
        (list as ArrayList<T>).removeAt(position)
        notifyItemRemoved(position)
    }

    fun removeLast() {
        (list as ArrayList<T>).removeAt(list.size - 1)
        notifyItemRemoved(list.size - 1)
    }

    fun removeItem(itemToRemove: T) {
        val positionOfItemToRemove = list.indexOf(itemToRemove)
        removeAt(positionOfItemToRemove)
    }

    fun insertItem(newListItem: T) {
        val listSize = list.size
        (list as ArrayList<T>).add(list.size, newListItem)
        notifyItemInserted(listSize)
    }

    fun insertItems(updatedList: List<T>) {
        (list as ArrayList<T>).addAll(updatedList)
        notifyDataSetChanged()
    }

    fun updateItem(updatedItem: T, updateIndex: Int) {
        (list as ArrayList<T>)[updateIndex] = updatedItem
        notifyItemChanged(updateIndex)
    }

    fun updateItems(updatedItem: List<T>, updateIndex: Int) {
        notifyItemRangeRemoved(updateIndex, list.size - 1)
        list = (list as ArrayList<T>).replaceItems(updateIndex, updatedItem)
        notifyItemRangeInserted(updateIndex, updatedItem.size)
    }

    fun updateItems(updatedItem: List<T>) {
        list = updatedItem
        notifyDataSetChanged()
    }

    private fun MutableList<T>.replaceItems(startIndex: Int, list: List<T>): MutableList<T> {
        val changeList = this.dropLast(size - 1).toMutableList()
        changeList.addAll(startIndex, list)
        return changeList
    }
}