package com.example.newsapp.feature

import android.os.Bundle
import com.example.newsapp.R
import com.example.newsapp.feature.base.BaseActivity
import com.example.newsapp.util.FragmentController

class HomeActivity : BaseActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_fragment_container)
        FragmentController.navigateTo(this, NewsCategoriesFragment::class.java.name, Bundle())
    }
}