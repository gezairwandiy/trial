package com.example.newsapp

import androidx.multidex.MultiDexApplication
import com.example.newsapp.service.apiRepository
import com.example.newsapp.util.appRepositoryModule
import com.example.newsapp.util.appViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext
import org.koin.core.context.startKoin

open class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        setupAppConfiguration()
    }

    fun setupAppConfiguration() {
        if (GlobalContext.getOrNull() == null) {
            startKoin {
                androidContext(this@App)
                modules(apiRepository, appViewModelModule, appRepositoryModule)
            }
        }
    }
}