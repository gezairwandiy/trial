package com.example.newsapp.service

import com.example.newsapp.model.SourceResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ApiService {

    @Headers("Content-Type: application/json")
    @GET("/sources")
    fun getSources(
        @Query("apiKey") key: String): Observable<SourceResponse>

}