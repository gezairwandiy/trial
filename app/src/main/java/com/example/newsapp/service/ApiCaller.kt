package com.example.newsapp.service

import com.example.newsapp.BuildConfig
import com.example.newsapp.model.SourceResponse
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import java.net.UnknownHostException

class ApiCaller (private val serviceApi: ApiService) {

    fun getSources(
        cbOnResult: (SourceResponse?) -> Unit,
        cbOnError: (Throwable?) -> Unit
    ) {
        getSourcesAPI().subscribeOn(Schedulers.io())
            .observeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableObserver<SourceResponse>() {
                override fun onComplete() {
                    dispose()
                }

                override fun onError(e: Throwable?) {
                    if (e is UnknownHostException) {
                        cbOnError(e)
                    } else {
                        cbOnError(Throwable("Unexpected Error"))
                    }
                    dispose()
                }

                override fun onNext(response: SourceResponse) {
                    cbOnResult(response)
                }
            })
    }

    private fun getSourcesAPI(): Observable<SourceResponse> {
        return serviceApi.getSources(BuildConfig.API_KEY)
    }
}