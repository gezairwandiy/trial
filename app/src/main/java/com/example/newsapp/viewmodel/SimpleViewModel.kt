package com.example.newsapp.viewmodel

import org.koin.core.KoinComponent

abstract class SimpleViewModel : KoinComponent {
    abstract fun layoutId(): Int
}