package com.example.newsapp.viewmodel

import android.app.Application
import com.example.newsapp.feature.base.BaseViewModel
import com.example.newsapp.model.Source
import com.example.newsapp.model.SourceResponse
import com.example.newsapp.service.ApiCaller
import org.koin.core.inject

class SourceViewModel(val app: Application) : BaseViewModel(app) {
    private val repository: ApiCaller by inject()
    var list = mutableListOf<SimpleViewModel>()

    fun getSources( cbOnSuccess: (response: SourceResponse?) -> Unit,
        cbOnError: (Throwable?) -> Unit
    ) {
        repository.getSources({
            it?.sources?.let { it1 -> getSourceList(it1) { cbOnSuccess(it) } }
        }, {
            cbOnError(it)
        })
    }

    fun getSourceList(forecasts: List<Source>, cb: () -> Unit) {
        list.clear()
        forecasts.forEachIndexed { index, it ->
            if (index != 0) {
                list.add(
                    SourceItemViewModel(
                        it.category
                    )
                )
            }
        }
        cb()
    }


}