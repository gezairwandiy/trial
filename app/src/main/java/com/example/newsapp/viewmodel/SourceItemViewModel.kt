package com.example.newsapp.viewmodel

import com.example.newsapp.R

class SourceItemViewModel (val category: String) :
    SimpleViewModel() {

    override fun layoutId(): Int = R.layout.view_source_item

}